package edu.uga.cs.ceren.assn1;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Count extends Configured implements Tool {

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new Count(), args);

		System.out.println("Finished! "+res);

		System.exit(res);
	}

	@Override
	public int run(String[] args) throws Exception {
		Job job = new Job(getConf(), "Count");
		job.setJarByClass(Count.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		job.setNumReduceTasks(3);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path("very_small_train.txt"));
		FileUtils.deleteDirectory(new File("output"));
		FileOutputFormat.setOutputPath(job, new Path("output"));

		job.waitForCompletion(true);

		return 0;
	}

	public static class Map extends
			Mapper<LongWritable, Text, Text, IntWritable> {
		private final static IntWritable ONE = new IntWritable(1);
		private Text word = new Text();

		static Vector<String> tokenizeDoc(String cur_doc) {
			String[] words = cur_doc.split("\\s+");
			Vector<String> tokens = new Vector<String>();
			for (int i = 0; i < words.length; i++) {
				words[i] = words[i].replaceAll("\\W", "");
				if (words[i].length() > 0) {
					tokens.add(words[i]);
				}
			}
			return tokens;
		}

		@Override
		public void map(LongWritable key, Text value, Context context)
				throws IOException, InterruptedException {
			//System.out.println("New entry: "+value.toString()+"...");
			String newLine = value.toString();
			String[] cat_val = newLine.split("\\t");
			//System.out.println("!! "+cat_val[0]);
			String[] cats = cat_val[0].split(",");
			Vector<String> docList = tokenizeDoc(cat_val[1]);
			for (String token : docList) {
				for (String cat : cats)
				{
					word.set(cat+":"+token);
					context.write(word, ONE);
				}
			}
		}
	}

	public static class Reduce extends
			Reducer<Text, IntWritable, Text, IntWritable> {
		@Override
		public void reduce(Text key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			context.write(key, new IntWritable(sum));
		}
	}

}
