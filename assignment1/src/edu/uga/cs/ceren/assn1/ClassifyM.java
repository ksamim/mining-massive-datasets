package edu.uga.cs.ceren.assn1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Vector;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

import edu.uga.cs.ceren.assn1.NaiveBayes.STAT_COUNTERS;

public class ClassifyM extends
		Mapper<LongWritable, Text, Text, Text> {
	
	private long V, DOM_Y, DOCS, CCAT, ECAT, GCAT, MCAT;
	
	private String[] labels = {"CCAT", "ECAT", "GCAT", "MCAT"};
	//private HashMap<String, Integer> countTable;
	private HashMap<String, Long> catMap;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		//Get all the classification statistics
		V = context.getConfiguration().getLong("V", 0);
		DOM_Y = context.getConfiguration().getLong("DOM_Y", 0);
		DOCS = context.getConfiguration().getLong("DOCS", 0);
		CCAT = context.getConfiguration().getLong("CCAT", 0);
		ECAT = context.getConfiguration().getLong("ECAT", 0);
		GCAT = context.getConfiguration().getLong("GCAT", 0);
		MCAT = context.getConfiguration().getLong("MCAT", 0);
		
		catMap = new HashMap<String, Long>();

		catMap.put("CCAT", CCAT);
		catMap.put("ECAT", ECAT);
		catMap.put("GCAT", GCAT);
		catMap.put("MCAT", MCAT);
	}

	@Override
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {

		String[] info = value.toString().split("\\t");
		String[] statistics = info[1].split(",");
		
		//If this is a label, keep track of its count - otherwise, we'll consider it when we have all the info
		HashMap<String, Integer> labelCount = new HashMap<String, Integer>();
		Vector<String> docs = new Vector<String>();
		for(String stat : statistics)
		{
			//Take out the documents
			if(stat.contains("::"))
			{
				docs.add(stat);
			}
			else
			{
				//System.out.println(stat);
				String[] statinfo = stat.split(":");
				labelCount.put(statinfo[0], Integer.parseInt(statinfo[1]));
			}
		}
		for(String doc : docs)
		{
			//Get document id, true categories, and number of times this word appeared
			String[] docInfo = doc.split("::");
			int docID = Integer.parseInt(docInfo[0]);
			String trueCat = docInfo[1];
			int wordCount = Integer.parseInt(docInfo[2]);
			
			String toReturn = "";
			Boolean started = false;
			//Create a list of Naive Bayes classification values for this word under all labels
			for(String label : labels)
			{
				if(started)
					toReturn+=",";
				double nextVal = 0;
				//If we don't have info for this label, it's easy -- the value is 0
				if(!labelCount.containsKey(label))
				{
					nextVal = Math.log((1.0/(double)V)/((double)catMap.get(label)+1.0));
				}
				else
				{
					nextVal = Math.log(((double)labelCount.get(label)+(1.0/(double)V))/((double)catMap.get(label)+1.0));
				}
				toReturn+=label+":"+(wordCount*nextVal);
				started=true;
			}
			context.write(new Text(docID+"::"+trueCat), new Text(toReturn));
		}
	}
}