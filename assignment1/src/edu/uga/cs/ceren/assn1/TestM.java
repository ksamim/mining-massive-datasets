package edu.uga.cs.ceren.assn1;

import java.io.IOException;
import java.util.Vector;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

import edu.uga.cs.ceren.assn1.NaiveBayes.STAT_COUNTERS;

public class TestM extends
		Mapper<LongWritable, Text, Text, Text> {

	private final static IntWritable ONE = new IntWritable(1);
	private Text word = new Text();

	static Vector<String> tokenizeDoc(String cur_doc) {
		String[] words = cur_doc.split("\\s+");
		Vector<String> tokens = new Vector<String>();
		for (int i = 0; i < words.length; i++) {
			words[i] = words[i].replaceAll("\\W", "");
			if (words[i].length() > 0) {
				tokens.add(words[i]);
			}
		}
		return tokens;
	}

	@Override
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		
		String newLine = value.toString();
		String[] cat_val = newLine.split("\\t");
		Vector<String> docList = tokenizeDoc(cat_val[1]);

		for (String token : docList) {
			context.write(new Text(token), new Text(key.get()+"::"+cat_val[0].replace(",", "-")));
		}
	}
}