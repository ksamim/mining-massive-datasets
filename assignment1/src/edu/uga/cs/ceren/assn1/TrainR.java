package edu.uga.cs.ceren.assn1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class TrainR extends
		Reducer<Text, Text, Text, Text> {
	@Override
	public void reduce(Text key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {
		context.getCounter(NaiveBayes.STAT_COUNTERS.V).increment(1);
		int sum = 0;
		//Create hashmap out of words to reduce counts of
		HashMap<String, Integer> wordCounts = new HashMap<String, Integer>();
		for (Text val : values) {
			String[] keyval = val.toString().split(":");
			if(wordCounts.get(keyval[0])==null)
				wordCounts.put(keyval[0], Integer.parseInt(keyval[1]));
			else
			{
				int curCount = wordCounts.get(keyval[0]);
				wordCounts.put(keyval[0], Integer.parseInt(keyval[1])+curCount);
			}
		}
		String toReturn="";
		Boolean addComma = false;
		for (Entry<String, Integer> tuple : wordCounts.entrySet())
		{
			if(addComma)
				toReturn+=",";
			toReturn+=tuple.getKey()+":"+tuple.getValue();
			addComma=true;
		}
		context.write(key, new Text(toReturn));
	}
}