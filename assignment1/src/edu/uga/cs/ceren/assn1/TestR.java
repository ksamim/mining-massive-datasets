package edu.uga.cs.ceren.assn1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class TestR extends
		Reducer<Text, Text, Text, Text> {
	@Override
	public void reduce(Text key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {
		String toReturn = "";
		Boolean started = false;
		Boolean hasTest = false;
		
		/*for(Text val : values)
		{
			String entry = val.toString();
			if(started)
				toReturn+=",";
			if(entry.contains("::"))
			{
				toReturn += entry+"::"+1;
				hasTest=true;
			}
			else
			{
				toReturn += entry;
			}
			started=true;
		}*/
		
		HashMap<String, Integer> countTest = new HashMap<String, Integer>();
		for (Text val : values) {
			if(val.toString().contains("::"))
			{
				//Don't add this yet, need to collapse duplicates
				if(!countTest.containsKey(val.toString()))
					countTest.put(val.toString(), 1);
				else
				{
					int curCount = countTest.get(val.toString());
					countTest.put(val.toString(), curCount+1);
				}
				continue;
			}
			else
			{
				if(started)
					toReturn+=",";
				toReturn += val.toString();
				started=true;
			}
		}
		//Don't spit anything out if we don't have any training data
		//if(toReturn.equals(""))
		//	return;
		
		//Boolean to check if this list has any test data
		hasTest = false;
		for(Entry<String, Integer> entry : countTest.entrySet())
		{
			hasTest = true;
			if(toReturn!="")
			{
				toReturn+=",";
			}
			toReturn+=entry.getKey()+"::"+entry.getValue();
		}
		if(hasTest)
			context.write(key, new Text(toReturn));
	}
}