package edu.uga.cs.ceren.assn1;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class NaiveBayes extends Configured implements Tool {
	
	public static enum STAT_COUNTERS {
		V,
		DOCS,
		CCAT,
		ECAT,
		GCAT,
		MCAT
	};

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new NaiveBayes(), args);

		System.exit(res);
	}

	@SuppressWarnings("deprecation")
	@Override
	public int run(String[] args) throws Exception {
		/**
		 * Variables
		 */

		FileUtils.deleteDirectory(new File("trainFolder"));
		FileUtils.deleteDirectory(new File("testFolder"));
		FileUtils.deleteDirectory(new File(getConf().get("output")));

		Path trainData = new Path(getConf().get("train"));
		Path testData = new Path(getConf().get("test"));
		Path trainFolder = new Path("trainFolder");
		Path testFolder = new Path("testFolder");
		Path output = new Path(getConf().get("output"));
		int reducerCount = getConf().getInt("reducers", 1);

		FileSystem.get(getConf()).delete(trainFolder);
		FileSystem.get(getConf()).delete(testFolder);
		FileSystem.get(getConf()).delete(output);
		/**
		 * Collect data from training set
		 */
		Job collect = new Job(getConf(), "NaiveBayes_collect");
		collect.setJarByClass(NaiveBayes.class);
		collect.setOutputKeyClass(Text.class);
		collect.setOutputValueClass(Text.class);

		collect.setNumReduceTasks(reducerCount);

		collect.setMapperClass(TrainM.class);
		collect.setReducerClass(TrainR.class);

		collect.setInputFormatClass(TextInputFormat.class);
		collect.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(collect, trainData);
		FileOutputFormat.setOutputPath(collect, trainFolder);

		collect.waitForCompletion(true);
		
		Counters counters = collect.getCounters();
		
		/**
		 * Store statistics
		 */
		//System.out.println("Total words: "+counters.findCounter(STAT_COUNTERS.V).getValue()+ "\nTotal documents: "+counters.findCounter(STAT_COUNTERS.DOCS).getValue());
		getConf().setLong("V", counters.findCounter(STAT_COUNTERS.V).getValue());
		getConf().setLong("DOM_Y", 4);
		getConf().setLong("DOCS", counters.findCounter(STAT_COUNTERS.DOCS).getValue());
		getConf().setLong("CCAT", counters.findCounter(STAT_COUNTERS.CCAT).getValue());
		getConf().setLong("GCAT", counters.findCounter(STAT_COUNTERS.GCAT).getValue());
		getConf().setLong("ECAT", counters.findCounter(STAT_COUNTERS.ECAT).getValue());
		getConf().setLong("MCAT", counters.findCounter(STAT_COUNTERS.MCAT).getValue());

		/**
		 * Get the test data, formatting it to keep track of document ID (offset), its correct labels, and the word count // REMOVED
		 */
		Job testAndMerge = new Job(getConf(), "NaiveBayes_testAndMerge");
		testAndMerge.setJarByClass(NaiveBayes.class);

		testAndMerge.setMapOutputKeyClass(Text.class);
        testAndMerge.setMapOutputValueClass(Text.class);
		testAndMerge.setOutputKeyClass(Text.class);
		testAndMerge.setOutputValueClass(Text.class);

		testAndMerge.setNumReduceTasks(reducerCount);

        MultipleInputs.addInputPath(testAndMerge, trainFolder, TextInputFormat.class,
                MergeM.class);
        MultipleInputs.addInputPath(testAndMerge, testData, TextInputFormat.class,
        		TestM.class);
		testAndMerge.setReducerClass(TestR.class);

		testAndMerge.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(testAndMerge, testData);
		FileOutputFormat.setOutputPath(testAndMerge, testFolder);

		testAndMerge.waitForCompletion(true);
		
		/**
		 * Take the merged data and calculate per-document log likelihoods for every label
		 */
        
		Job classify = new Job(getConf(), "NaiveBayes_classify");
		classify.setJarByClass(NaiveBayes.class);
		classify.setOutputKeyClass(Text.class);
		classify.setOutputValueClass(Text.class);

		classify.setNumReduceTasks(reducerCount);

		classify.setMapperClass(ClassifyM.class);
		classify.setReducerClass(ClassifyR.class);

		classify.setInputFormatClass(TextInputFormat.class);
		classify.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(classify, testFolder);
		FileOutputFormat.setOutputPath(classify, output);

		classify.waitForCompletion(true);
		
		/**
		 * Count our wins and losses, report our accuracy
		 */
		int wins = 0, total = 0;
		Path winCounts = new Path(output, "part-r-[0-9]*");
		FileSystem fs = output.getFileSystem(getConf());
        FileStatus[] wL = fs.globStatus(winCounts);
        for (FileStatus performance : wL) {
            FSDataInputStream inputStream = fs.open(performance.getPath());
			InputStreamReader betterReader = new InputStreamReader(inputStream);
			BufferedReader buf= new BufferedReader(betterReader);
			
            String newLine="";
			while((newLine = buf.readLine())!=null)
			{
				String[] process = newLine.split("\\t");
				String[] classifyInfo = process[0].split("::");
				System.out.println("["+classifyInfo[1].replace("-",", ")+"]\t"+classifyInfo[2]+"\t"+classifyInfo[3]);
                wins += Integer.parseInt(process[1]);
                ++total;
            }
        }
		System.out.format("Percent correct: %d/%d = %.2f%%\n",wins,total,((double)wins/(double)total)*100);
		
		return 0;
	}

	

}
