package edu.uga.cs.ceren.assn1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class ClassifyR extends
		Reducer<Text, Text, Text, IntWritable> {

	private String[] labels = {"CCAT", "ECAT", "GCAT", "MCAT"};
	private long V, DOM_Y, DOCS, CCAT, ECAT, GCAT, MCAT;
	private HashMap<String, Long> catMap;
	
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		//Get all the classification statistics
		V = context.getConfiguration().getLong("V", 0);
		DOM_Y = context.getConfiguration().getLong("DOM_Y", 0);
		DOCS = context.getConfiguration().getLong("DOCS", 0);
		CCAT = context.getConfiguration().getLong("CCAT", 0);
		ECAT = context.getConfiguration().getLong("ECAT", 0);
		GCAT = context.getConfiguration().getLong("GCAT", 0);
		MCAT = context.getConfiguration().getLong("MCAT", 0);
		
		catMap = new HashMap<String, Long>();

		catMap.put("CCAT", CCAT);
		catMap.put("ECAT", ECAT);
		catMap.put("GCAT", GCAT);
		catMap.put("MCAT", MCAT);
	}
	@Override
	public void reduce(Text key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {
		//double sum = 0;
		//First, find the most probable classification
		HashMap<String, Double> calcMap = new HashMap<String, Double>();

		calcMap.put("CCAT", 0.0);
		calcMap.put("ECAT", 0.0);
		calcMap.put("GCAT", 0.0);
		calcMap.put("MCAT", 0.0);
		
		for (Text val : values) {
			//First, parse the list of labels
			String[] labelVals = val.toString().split(",");
			//Then, put their values in the correct hash values
			for(String lVals : labelVals)
			{
				String[] bucketVals = lVals.split(":");
				String bucket = bucketVals[0];
				double bcount = Double.parseDouble(bucketVals[1]);
				
				//Get the original value
				double curCalc = calcMap.get(bucket);
				calcMap.put(bucket, bcount + curCalc);
			}
		}
		
		//Finish of the Naive Bayes classifier
		for(String label : labels)
		{
			calcMap.put(label, calcMap.get(label)+Math.log(((double)catMap.get(label)+(1/(double)DOM_Y))/((double)DOCS+1)));
		}
		
		double best=-99999999;
		String winner = "";
		for(Entry<String, Double> entry : calcMap.entrySet())
		{
			if(entry.getValue()>best)
			{
				winner = entry.getKey();
				best = entry.getValue();
			}
		}
		
		int correct = 0;
		
		String[] correctLabels = key.toString().split("::")[1].split("-");
		
		for(String label : correctLabels)
		{
			if(winner.equals(label))
				correct = 1;
		}
		
		context.write(new Text(key.toString()+"::"+winner+"::"+best), new IntWritable(correct));
		//sum += Math.log(((double)catMap.get(key.toString().split(":")[2])+(1/(double)DOM_Y))/((double)DOCS+1));
		
		//System.out.println(key.toString().split(":")[2]);
		//System.out.println("\t"+sum+"+="+Math.log(((double)catMap.get(key.toString().split(":")[2])+(1/(double)DOM_Y))/((double)DOCS+1)));
		//System.out.println("\t"+(double)catMap.get(key.toString().split(":")[2])+(1/(double)DOM_Y));
		//System.out.println("\t"+(double)DOCS+1);
		//System.out.println("\t"+((double)catMap.get(key.toString().split(":")[2])+(1/(double)DOM_Y))/((double)DOCS+1));
	}
}