package edu.uga.cs.ceren.assn1;

import java.io.IOException;
import java.util.Vector;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

import edu.uga.cs.ceren.assn1.NaiveBayes.STAT_COUNTERS;

public class MergeM extends
		Mapper<LongWritable, Text, Text, Text> {

	@Override
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		String newLine = value.toString();
		context.getCounter(NaiveBayes.STAT_COUNTERS.DOCS).increment(1);
		String[] cat_val = newLine.split("\\t");
		
		//System.out.println("!!");
		//System.out.println("Merge file: "+cat_val[0]+", "+cat_val[1]);
		
		context.write(new Text(cat_val[0]), new Text(cat_val[1]));
	}
}