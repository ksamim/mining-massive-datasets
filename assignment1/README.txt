Name: Roi Ceren
Project: Assignment #1 - NaiveBayes
Email: roi.ceren@gmail.com

Run with: hadoop jar assignment1.jar -D reducers=<# reducers> -D train=s3://path/to/train/file -D test=s3://path/to/test/file -D output=/path/to/output/folder

Question 1: Please see assignment1.jpg for the graph. Increasing the number of reducers does not correspondingly reduce the runtime to a fraction of its previous runtime, but
there is quite a bit of difference. Considering that some of the non-linearity is likely due to stochasticity in the AWS day-to-day performance, it seems that there is a linear
decrease in runtime by increasing the number of reducers. However, the reduction in runtime is not as significant as the increase in reducer nodes.

Question 2: The most obvious approach would be to, instead of considering each word separately under each label, consider additionally multiple labels together. For example,
if the word "investors" appeared in both MCAT and ECAT, increment MCAT:investors, ECAT:investors, and ECAT,MCAT:investors. When doing classification, consider not only the
word for each individual y', but also combinations of several categories.

Program outputs:

- RCV1.very_small_*

[GCAT]	GCAT	-1215.403737964642
[C13, CCAT, GCAT, GHEA]	CCAT	-460.1551571884011
[C11, C13, CCAT, E12, ECAT, M13, M132, MCAT]	ECAT	-1045.354231857372
[C24, CCAT, M14, MCAT]	GCAT	-8065.818051073395
[C15, C152, C18, C181, CCAT]	CCAT	-549.8115593213834
[C13, CCAT, M11, MCAT]	CCAT	-702.9517013639419
[C31, CCAT]	CCAT	-1400.7620024054115
[E51, E512, ECAT, GCAT, GDIP]	ECAT	-2516.1108715780906
Percent correct: 7/8 = 87.50%

- RCV1.small_*

Percent correct: 664/818 = 81.17%

- RCV1.full_*

Percent correct: 65024/80435 = 80.84%
