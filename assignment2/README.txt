Name: Roi Ceren
Project: Assignment #2 - KMeans
Email: roi.ceren@gmail.com

You must specify the number of clusters to identify, a set of data to cluster, and an initial set of centroids.

	> hadoop jar assignment2.jar -D clusters=<cluster number> -D data=s3://path/to/gist/file.txt -D centroids=s3://path/to/init/cluster/file.txt
	
Optionally, you can provide a number of reducers (default 10), an update delta (such that changes less than 10^-d are still considered converged, default 5), and a max iterations (default 100).

	> ...  -D reducers=<num reducers> -D delta=<update threshold, 10^-delta> -D iterations=<max iterations> 

---------
PROBLEM 1
---------

a) 10 clusters, threshold of 10^-5

Iterations: 54
Runtime: 16 m, 12 s

Centroid 1: 19390234
Centroid 2: 6379034
Centroid 3: 4138175
Centroid 4: 16033113
Centroid 5: 20034909
Centroid 6: 20577504
Centroid 7: 17902356
Centroid 8: 4447234
Centroid 9: 13087542
Centroid 10: 5576892

b) 50 clusters, threshold of 10^-5

Iterations: 22
Runtime: 16 m, 33 s

Centroid 1: 17902356
Centroid 2: 9720379
Centroid 3: 1762655
Centroid 4: 839258
Centroid 5: 12238087
Centroid 6: 11341983
Centroid 7: 10755532
Centroid 8: 12193722
Centroid 9: 2324158
Centroid 10: 12530159
Centroid 11: 1397460
Centroid 12: 7401444
Centroid 13: 5513633
Centroid 14: 20959332
Centroid 15: 4661482
Centroid 16: 49425
Centroid 17: 12040005
Centroid 18: 6630022
Centroid 19: 927538
Centroid 20: 150126
Centroid 21: 20535477
Centroid 22: 15373263
Centroid 23: 4484907
Centroid 24: 4238041
Centroid 25: 7337034
Centroid 26: 11279341
Centroid 27: 7041411
Centroid 28: 3385201
Centroid 29: 11711054
Centroid 30: 3707210
Centroid 31: 14542261
Centroid 32: 14243907
Centroid 33: 1305148
Centroid 34: 5918813
Centroid 35: 16861600
Centroid 36: 14313299
Centroid 37: 4138175
Centroid 38: 18764082
Centroid 39: 20109708
Centroid 40: 2568877
Centroid 41: 346267
Centroid 42: 20911358
Centroid 43: 4552821
Centroid 44: 527593
Centroid 45: 10328183
Centroid 46: 6091213
Centroid 47: 16227287
Centroid 48: 20958598
Centroid 49: 4411559
Centroid 50: 18229916


---------
PROBLEM 2
---------

a) 10 clusters, threshold of 10^-5, max iterations of 10, 10 machines on AWS

FAILED TO CONVERGE IN 10 ITERATIONS
Iterations: 10 (CHANGED: incorrect indexing on original submission)
Runtime: 1127 m, 44 s

Centroid 1: 15722795
Centroid 2: 20591106
Centroid 3: 7322244
Centroid 4: 13293434
Centroid 5: 17269847
Centroid 6: 14001151
Centroid 7: 7730521
Centroid 8: 2158378
Centroid 9: 2908088
Centroid 10: 6688098

b) 50 clusters, threshold of 10^-5, max iterations of 10, 20 machines on AWS

(CHANGED: completed late run on AWS)

FAILED TO CONVERGE IN 10 ITERATIONS
Iterations: 10
Runtime: 1468 m, 10 s

Centroid 1: 15851669
Centroid 2: 18787339
Centroid 3: 7196125
Centroid 4: 10957329
Centroid 5: 16397135
Centroid 6: 2861304
Centroid 7: 2710105
Centroid 8: 5155717
Centroid 9: 4989601
Centroid 10: 18287253
Centroid 11: 18517300
Centroid 12: 13772288
Centroid 13: 9512185
Centroid 14: 7846615
Centroid 15: 18800019
Centroid 16: 9485029
Centroid 17: 13348329
Centroid 18: 9523340
Centroid 19: 3736365
Centroid 20: 10838403
Centroid 21: 2908088
Centroid 22: 20581261
Centroid 23: 19083761
Centroid 24: 15886798
Centroid 25: 17276766
Centroid 26: 19759723
Centroid 27: 13140151
Centroid 28: 18779101
Centroid 29: 15568672
Centroid 30: 20677421
Centroid 31: 8945281
Centroid 32: 244736
Centroid 33: 1860261
Centroid 34: 3622992
Centroid 35: 18520393
Centroid 36: 6612187
Centroid 37: 12273391
Centroid 38: 7403516
Centroid 39: 9213911
Centroid 40: 14230041
Centroid 41: 20121854
Centroid 42: 20583109
Centroid 43: 15362486
Centroid 44: 20845109
Centroid 45: 11065721
Centroid 46: 17325807
Centroid 47: 7162319
Centroid 48: 14888127
Centroid 49: 8055141
Centroid 50: 16731180

---------
PROBLEM 3
---------

Since the gist vectors are already compressed representations of the features within the data, it is quite difficult to use a distance metric that is more naive than Euclidean distance.
We might be tempted to utilize an approach such as the number of similar values in the dimensions for two data points, but due to the high granularity in gist vector values, it is likely
our thresholds will be far too tight. One optimization may be to eliminate large portions of the gist vector, only considering, say, the middle 50% of data points.

Partitioning the gist vectors into canopies using Euclidean distance is straightforward. Select some loose threshold t1, and a tighter threshold t2. These gist vectors appear bounded between
0 and 1 (or perhaps even as low as 0.5) per element in the vector. The outer ring of a canopy might be 0.1, while the inner ring is 0.01. Selecting a random data point initially, if any
other data point is within t1 of this data point, they are considered in the same canopy. If it is within t2, then it is additionally unable to be the source of another canopy. By doing this,
we eventually eliminate all potential data points for canopies, and any two data points that are not in the same canopy will not be compared further.

Besides Euclidean distance, one very loose canopy selection mechanism mechanism might be to compare the sums of all elements in a gist vector against one another, using the same methodology
of selecting two thresholds and incrementally eliminating candidates for canopy creation.

---------
PROBLEM 4
---------

Canopy clustering can certainly be implemented in MapReduce. The goal of the Mapper, which receives a small partition of the data set, is to identify potential canopy centers (initially with
a stochastic choice of the first canopy center) within the current data points. As with the canopy selection algorithm, data within some user-defined loose threshold are all considered in
the same "canopy", and cannot be a canopy itself if within a tighter user-defined threshold. Repeat this process in the Mapper until no data is outside a canopy.

The Reducer must resolve conflicts between the disparate Mapper canopy choices. Specifically, no canopy center should be within the inner threshold of another canopy. The elimination of
canopy centers may leave some data points outside any canopy, which might be intentional (outlier data) or might need to be resolved (loop MR until even outliers are covered, potentially by
their own canopy). In Assignment 2, we are requested to adhere to a user-defined number of centroids, so some post-processing may be required to eliminate canopies. Additionally, to benefit
from canopy selection, a file or data structure associating each data point to the clusters it falls under (perhaps by reformatting the data itself) is necessary., 

---------
PROBLEM 5
---------

Canopy clustering is intentionally designed with the inclusion of more precise clustering methods in mind. The canopies that are created in Problem 4 are used as initial centroids for K-Means.
In Assignment 2, we were provided an initial file with centroids, it would be trivial to output the centroids chosen in Problem 4 to /files/kmeans/iter_0/ prior to starting the K-Means
classification loop. Simply, we run canopy clustering before KMeans, and use Canopy's output as initial centers for KMeans.

An additional modification will be required to benefit from the canopies, however. In Problem 4, data points are modified to include references to centroids (previously canopies) it falls
under. In this way, when recalculating membership to a centroid for a data point, we instead limit the Euclidean distance calculations only to clustroids that had a canopy the data point
fell under. In this way, we dramatically reduce the amount of calculations required, as many data points will fall under few canopies.