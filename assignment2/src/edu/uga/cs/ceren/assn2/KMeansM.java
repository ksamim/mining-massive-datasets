package edu.uga.cs.ceren.assn2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Vector;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;


public class KMeansM extends Mapper<LongWritable, Text, LongWritable, Text> {
	
	private HashMap<Long, Vector<Double>> centroids;
	
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		URI[] localPaths = context.getCacheFiles();
		Path centroidData = new Path(localPaths[0].getPath());
		centroids = new HashMap<Long, Vector<Double>>();
		
		//Unceremoniously ripped from: https://sites.google.com/site/hadoopandhive/home/how-to-read-all-files-in-a-directory-in-hdfs-using-hadoop-filesystem-api
		FileSystem fs = FileSystem.get(new Configuration());
        FileStatus[] status = fs.listStatus(centroidData);
        for (int i=0;i<status.length;++i){
                BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
                String line;
                line=br.readLine();
                while (line != null){
                	String[] tabSplit = line.split("\t");
                	
                	//If there's best data point info, ignore it
                	String centroidD = tabSplit[1];
                	if(centroidD.contains("::"))
                	{
                		centroidD = centroidD.split("::")[0];
                	}
                	
                	String[] commaSplit = centroidD.split(",");
                	Vector<Double> centroidInfo = new Vector<Double>();
                	for(int j=0;j<commaSplit.length;++j)
                	{
                		centroidInfo.add(Double.parseDouble(commaSplit[j]));
                	}
                	//System.out.println(centroidInfo.size());
                	centroids.put(Long.parseLong(tabSplit[0]), centroidInfo);
                    line=br.readLine();
                }
        }
	}
	
	double euclideanDistance(String[] p, Vector<Double> q)
	{
		double innerSum = 0.0;
		for(int i=0;i<p.length;++i)
		{
			innerSum += Math.pow(Double.parseDouble(p[i])-q.get(i), 2.0);
		}
		return Math.sqrt(innerSum);
	}
	
	long findCluster(String[] dataPoint)
	{
		boolean started = false;
		long bestCluster = 0;
		double bestDistance = 0;
		for(Entry<Long, Vector<Double>> cluster : centroids.entrySet())
		{
			if(!started)
			{
				bestCluster = cluster.getKey();
				bestDistance = euclideanDistance(dataPoint, cluster.getValue());
				started = true;
			}
			else {
				long curCluster = cluster.getKey();
				double curDistance = euclideanDistance(dataPoint, cluster.getValue());
				
				if(curDistance < bestDistance)
				{
					bestCluster = curCluster;
					bestDistance = curDistance;
				}
			}
		}
		return bestCluster;
	}
	
	@Override
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		
		String newLine = value.toString();
		String[] cat_val = newLine.split("\t");
		long dataID = Long.parseLong(cat_val[0]);
		String[] entry = cat_val[1].split(",");
		
		long clusterID = findCluster(entry);
		
		//System.out.println(dataID + " -> " + clusterID);
		
		context.write(new LongWritable(clusterID), new Text(dataID+":"+euclideanDistance(entry,centroids.get(clusterID))+":::"+cat_val[1]));
		
		//Commit some empty vectors for the other clusters, in case they end up empty
		int clusterCount = Integer.parseInt(context.getConfiguration().get("clusterCount"));
		for(int i=0;i<clusterCount;++i)
		{
			if((long)i != clusterID)
			{
				context.write(new LongWritable(clusterID), new Text(""));
			}
		}
	}

}
