package edu.uga.cs.ceren.assn2;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class KMeans extends Configured implements Tool {
	
	public static enum KMEANS_COUNTERS {
		CONVERGED
	};

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new KMeans(), args);

		System.exit(res);
	}

	@SuppressWarnings("deprecation")
	@Override
	public int run(String[] args) throws Exception {
		/**
		 * Variables
		 */
		int clusterCount = getConf().getInt("clusters", 10);
		int reducers = getConf().getInt("reducers", 10);
		int maxItr = getConf().getInt("iterations",100);
		int delta = getConf().getInt("delta",5);
		Path data = new Path(getConf().get("data"));
		Path centroids = new Path(getConf().get("centroids"));
		//Path output = new Path(getConf().get("output"));

		//FileSystem.get(getConf()).delete(output);
		
		//Load up the initial configuration folder
		Job initcluster = new Job(getConf(), "KMeans_initCluster");
		initcluster.setJarByClass(KMeans.class);

		initcluster.setMapOutputKeyClass(Text.class);
		initcluster.setMapOutputValueClass(Text.class);
		initcluster.setOutputKeyClass(Text.class);
		initcluster.setOutputValueClass(Text.class);

		initcluster.setNumReduceTasks(0); //Is this kosher?
		initcluster.setMapperClass(KMeansInitCluster.class);

		initcluster.setInputFormatClass(TextInputFormat.class);
		initcluster.setOutputFormatClass(TextOutputFormat.class);
		
		FileSystem.get(getConf()).delete(new Path("files"),true);
		
		Path initc = new Path("files/kmeans/iter_0");
		
		FileSystem.get(getConf()).delete(initc);

		FileInputFormat.addInputPath(initcluster, centroids);
		FileOutputFormat.setOutputPath(initcluster, initc);

		initcluster.waitForCompletion(true);

		int iter = 1;
		long centroidChange = 0;
		
		long startTime = System.currentTimeMillis();
		
		do{
			Configuration conf = new Configuration();
			conf.set("loops.iter", iter+"");
			conf.set("delta", delta+"");
			conf.set("clusterCount", clusterCount+"");
			Job recluster = new Job(conf, "KMeans_Recluster_"+iter);
			recluster.setJarByClass(KMeans.class);

			recluster.setMapOutputKeyClass(LongWritable.class);
			recluster.setMapOutputValueClass(Text.class);
			recluster.setOutputKeyClass(LongWritable.class);
			recluster.setOutputValueClass(Text.class);
	
			recluster.setNumReduceTasks(reducers);
	
			recluster.setMapperClass(KMeansM.class);
			recluster.setCombinerClass(KMeansC.class);
			recluster.setReducerClass(KMeansR.class);
	
			recluster.setInputFormatClass(TextInputFormat.class);
			recluster.setOutputFormatClass(TextOutputFormat.class);
			
			//Previous and next iterations
			Path in = new Path("files/kmeans/iter_" + (iter - 1) + "/");
			Path out = new Path("files/kmeans/iter_" + iter);
			
			FileSystem.get(getConf()).delete(out);
			
			//Pass the previous centroids as DistributedCache
	
			//Read in the data every time, add the last iteration to cache
			recluster.addCacheFile(in.toUri());
			FileInputFormat.addInputPath(recluster, data);
			FileOutputFormat.setOutputPath(recluster, out);
	
			recluster.waitForCompletion(true);
			
			Counters counters = recluster.getCounters();
			
			//Check to see how many centroids have moved
			centroidChange = counters.findCounter(KMEANS_COUNTERS.CONVERGED).getValue();
			
			//System.out.println(centroidChange);
			
			++iter;
		} while(centroidChange > 0 && iter<maxItr);
		
		//Print out each centroid ID and the best nearest data ID
		System.out.println("Finished, dumping data\n--------------------------------------------------------------------");
		
		if(centroidChange>0)
		{
			System.out.println("FAILED TO CONVERGE IN "+maxItr+" ITERATIONS");
		}
		
		//Dump iterations
		System.out.println("Iterations: "+(iter-1));
		
		//Dump runtime
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(String.format("Runtime: %d m, %d s", 
			    TimeUnit.MILLISECONDS.toMinutes(totalTime),
			    TimeUnit.MILLISECONDS.toSeconds(totalTime) - 
			    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalTime))
			));
		
		//Dump nearest images to centroids
		Path centroidData = new Path("files/kmeans/iter_"+(iter-1)+"/");
		HashMap<Long, Long> centHM = new HashMap<Long, Long>();
		
		//Unceremoniously ripped from: https://sites.google.com/site/hadoopandhive/home/how-to-read-all-files-in-a-directory-in-hdfs-using-hadoop-filesystem-api
		FileSystem fs = FileSystem.get(new Configuration());
        FileStatus[] status = fs.listStatus(centroidData);
        for (int i=0;i<status.length;++i){
                BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
                String line;
                line=br.readLine();
                while (line != null){
                	String[] tabSplit = line.split("\t");
                	
                	//Get the best data point info
                	String closest = tabSplit[1].split("::")[1].split(":")[0];
                	
                	centHM.put(Long.parseLong(tabSplit[0]), Long.parseLong(closest));
                    line=br.readLine();
                }
        }
        
        for(long i=0;i<(long)clusterCount;++i)
        {
        	System.out.println("Centroid "+(i+1)+": "+centHM.get(i));
        }
		
		return 0;
	}
}


/*Load the data
Job initdata = new Job(getConf(), "KMeans_initData");
initdata.setJarByClass(KMeans.class);

initdata.setMapOutputKeyClass(Text.class);
initdata.setMapOutputValueClass(Text.class);
initdata.setOutputKeyClass(Text.class);
initdata.setOutputValueClass(Text.class);

initdata.setNumReduceTasks(0); //Is this kosher?
initdata.setMapperClass(KMeansInitData.class);

initdata.setInputFormatClass(TextInputFormat.class);
initdata.setOutputFormatClass(TextOutputFormat.class);

Path initd = new Path("files/kmeans/data");

FileSystem.get(getConf()).delete(initc);

FileInputFormat.addInputPath(initdata, data);
FileOutputFormat.setOutputPath(initdata, initd);

initdata.waitForCompletion(true);*/
