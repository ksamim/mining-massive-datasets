package edu.uga.cs.ceren.assn2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class KMeansR extends Reducer<LongWritable, Text, LongWritable, Text> {
	
	private HashMap<Long, Vector<Double>> centroids;
	private HashMap<Long, String> oldInfo;
	
	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		URI[] localPaths = context.getCacheFiles();
		Path centroidData = new Path(localPaths[0].getPath());
		centroids = new HashMap<Long, Vector<Double>>();
		oldInfo = new HashMap<Long, String>();
		
		//Unceremoniously ripped from: https://sites.google.com/site/hadoopandhive/home/how-to-read-all-files-in-a-directory-in-hdfs-using-hadoop-filesystem-api
		FileSystem fs = FileSystem.get(new Configuration());
        FileStatus[] status = fs.listStatus(centroidData);
        for (int i=0;i<status.length;++i){
                BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
                String line;
                line=br.readLine();
                while (line != null){
                	String[] tabSplit = line.split("\t");
                	
                	//Store old information
                	oldInfo.put(Long.parseLong(tabSplit[0]), tabSplit[1]);
                	
                	//If there's best data point info, ignore it
                	String centroidD = tabSplit[1];
                	if(centroidD.contains("::"))
                	{
                		centroidD = centroidD.split("::")[0];
                	}
                	
                	String[] commaSplit = centroidD.split(",");
                	Vector<Double> centroidInfo = new Vector<Double>();
                	for(int j=0;j<commaSplit.length;++j)
                	{
                		centroidInfo.add(Double.parseDouble(commaSplit[j]));
                	}
                	//System.out.println(centroidInfo.size());
                	centroids.put(Long.parseLong(tabSplit[0]), centroidInfo);
                    line=br.readLine();
                }
        }
	}

	@Override
	public void reduce(LongWritable key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {

		Vector<Double> newCentroid = new Vector<Double>();
		boolean started = false;
		int pointCount = 0;
		
		long bestDataPoint = 0;
		double bestDistance = 0.0;
		
		double threshold = Math.pow(10, -1*Double.parseDouble(context.getConfiguration().get("delta")));
		
		for(Text value : values)
		{
			//If value is an empty vector, skip it
			if(value.toString().equals(""))
				continue;
			
			String[] dataPrune = value.toString().split(":::");
			
			//Deal with pruned data point for question 1/2
			String[] dataPoint = dataPrune[0].split(":");
			long curDataPoint = Long.parseLong(dataPoint[0]);
			double curDistance = Double.parseDouble(dataPoint[1]);
			if(curDistance < bestDistance)
			{
				bestDataPoint = curDataPoint;
				bestDistance = curDistance;
			}
			
			int multiplier = 1;
			String imageInfo = dataPrune[1];
			
			//Check if combined
			if(imageInfo.contains("::"))
			{
				String[] combinePrune = imageInfo.split("::");
				multiplier = Integer.parseInt(combinePrune[1]);
				imageInfo = combinePrune[0];
			}
			
			String[] dimensions = imageInfo.split(",");
			if(!started)
			{
				bestDataPoint = curDataPoint;
				bestDistance = curDistance;
				
				for(int i=0;i<dimensions.length;++i)
				{
					newCentroid.add(Double.parseDouble(dimensions[i])*(double)multiplier);
				}
				pointCount+=multiplier;
				started = true;
			}
			else {
				for(int i=0;i<dimensions.length;++i)
				{
					newCentroid.set(i, newCentroid.get(i)+Double.parseDouble(dimensions[i])*(double)multiplier);
				}
				pointCount+=multiplier;
			}
		}
		
		//If there's no change, repeat old info, otherwise change
		if(newCentroid.size()!=0)
		{
			//Fix centroid info
			for(int i=0;i<newCentroid.size();++i)
			{
				newCentroid.set(i, newCentroid.get(i)/(double)pointCount);
			}
			
			//Check if centroid has changed, also prepare centroid for writing
			Vector<Double> compare = centroids.get(key.get());
			String centroidChange = "";
			boolean changed = false;
			for(int i=0;i<compare.size();++i)
			{
				if(!changed && Math.abs(newCentroid.get(i)-compare.get(i))>threshold)
				{
					context.getCounter(KMeans.KMEANS_COUNTERS.CONVERGED).increment(1);
					changed = true;
				}
				if(!centroidChange.equals(""))
					centroidChange+=",";
				centroidChange+=newCentroid.get(i);
			}
			
			//Add best image information
			centroidChange+="::"+bestDataPoint+":"+bestDistance;
			
			//Commit new centroid
			context.write(key, new Text(centroidChange));
		}
		else
		{
			context.write(key, new Text(oldInfo.get(key.get())));
		}
	}

}
