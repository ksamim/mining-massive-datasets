package edu.uga.cs.ceren.assn2;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class KMeansInitCluster extends Mapper<LongWritable, Text, IntWritable, Text> {
	
	@Override
	public void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		String newline = value.toString();
		String[] cluster = newline.split("\t");
		
		context.write(new IntWritable(Integer.parseInt(cluster[0])), new Text(cluster[1]));
	}

}
