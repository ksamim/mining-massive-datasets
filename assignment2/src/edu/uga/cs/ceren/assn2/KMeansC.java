package edu.uga.cs.ceren.assn2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;


public class KMeansC extends Reducer<LongWritable, Text, LongWritable, Text> {
	
	@Override
	public void reduce(LongWritable key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {
		
		Vector<Double> newCentroid = new Vector<Double>();
		boolean started = false;
		int pointCount = 0;
		
		long bestDataPoint = 0;
		double bestDistance = 0.0;
		
		for(Text value : values)
		{
			//If value is an empty vector, skip it
			if(value.toString().equals(""))
				continue;
			
			String[] dataPrune = value.toString().split(":::");
			
			//Deal with pruned data point for question 1/2
			String[] dataPoint = dataPrune[0].split(":");
			long curDataPoint = Long.parseLong(dataPoint[0]);
			double curDistance = Double.parseDouble(dataPoint[1]);
			if(curDistance < bestDistance)
			{
				bestDataPoint = curDataPoint;
				bestDistance = curDistance;
			}
			
			//System.out.println("[Combiner] "+bestDataPoint + " : " + bestDistance);
			
			String[] dimensions = dataPrune[1].split(",");
			if(!started)
			{
				bestDataPoint = curDataPoint;
				bestDistance = curDistance;
				
				for(int i=0;i<dimensions.length;++i)
				{
					newCentroid.add(Double.parseDouble(dimensions[i]));
				}
				++pointCount;
				started = true;
			}
			else {
				for(int i=0;i<dimensions.length;++i)
				{
					newCentroid.set(i, newCentroid.get(i)+Double.parseDouble(dimensions[i]));
				}
				++pointCount;
			}
		}
		
		String interim = "";
		
		for(int i=0;i<newCentroid.size();++i)
		{
			newCentroid.set(i, newCentroid.get(i)/(double)pointCount);
			if(!interim.equals(""))
				interim+=",";
			interim+=newCentroid.get(i);
		}
		
		if(interim.equals(""))
			context.write(key, new Text(""));
		else
			context.write(key, new Text(bestDataPoint+":"+bestDistance+":::"+interim+"::"+pointCount));
	}
}
