Name: Roi Ceren
Project: Assignment #3 - Singular Value Decomposition (or, How To Be a Wizard)
Email: roi.ceren@gmail.com

In order to run this project, use the following command. Do not include a following slash on the data folder

> java -jar assignment3.jar data=/path/to/data/folder

Several flags are additionally available for more fun and wizardry. You can define --master local[*] as normal, with a
 variety of cores selected. You may provide one or more k values (comma separated, no spaces, default 10). You can also
 print out the top k span values (default 0, do not print). Lastly, you may print out the Frobineus norm difference
 between the true average and the SVD averages provided in the list of ks by including norms=1 (default 0).

> ... --master local[*] k=#,#,...# kSpans=# norms={0,1}

---------
Problem 1
---------

1 core (k=10): 22 minutes, 13 seconds
4 core (k=10): 19 minutes, 13 seconds

Timing was done only over the parallelized portion of the algorithm, as it is obvious more cores will not significantly
improve overhead. Even so, 4 cores is not 1/4th of the computation time. However, it is faster. It's easy to assume that
increasing cores across many machines would further improve the runtime, but the overhead would certainly catch up at
some point, similar to our observations on Hadoop.

---------
Problem 2
---------

Images are saved as p2_<k>.png, with the original average as p2_999.png. Below are the values of ||A-A_k||_F for values
k={10, 50, 100}.

Norm for k=10:  240.22378036042159
Norm for k=50:  151.16086151792638
Norm for k=100: 97.81990424337357

As to be expected, the more dimensions that we leave in, the smaller the Frobineus norm will be. We would like to select
a k that is much lower than the amount of dimensions, but additionally has a low Frobineus norm between itself and the
true average.

---------
Problem 3
---------

Below are the pixels with the maximum span, labeled: x,y: value

This can be reprinted with the parameter printSpan=1

5,67: 255
8,72: 255
50,41: 255
112,379: 255
171,412: 255
400,0: 255
335,183: 252
258,123: 251
260,227: 251
305,3: 251

---------
Problem 4
---------

Utilizing the MapReduce algorithm presented in Distributed Approximate Spectral Clustering for Large-Scale Datasets, we
 will first perform LSH on the grayscale image vectors in the data set in the Mapper using a hyperplane vector explained
 below.

The Mapper requires an array of hyperplanes, which is a subset of the total dimensions in the image, and a threshold for
 each hyperplane. The hyperplanes are chosen by the spans calculated in the method created for Problem 3. The
 probability of selecting a dimension to be a hyperplane is the span of that dimension divided by the total sum of all
 dimensions' spans. The threshold is determined given some bin size (authors selected 20). bin[j] represents the amount
 of images that, for dimension i, have an associated value on that index between min[i] + j*span[i] and min[i] + (j+1)*
 span[i]. Taking the minimum value for all the bins, denoted s, the threshold for that hyperplane of dimension i is:
 min[i] + s*span[i]/20. The output from the mapper is a binary signature (determined by examining every hyperplane and
 whether the image on that dimension exceeds the threshold) and the associated image identifier.

Prior to invoking the reducer, we'd like to merge some of the signatures, since there can be quite a few (2^M where M is
 the number of hyperplanes). Simply, merge all signatures that share some P<=M binary values into the same bucket.

In the Reducer, we simply compute the subsimilarity matrix of all images in the same bucket. Afterwards, we take all
 output matrices and compare them to all other matrices. If two matrices are similar, we combine the buckets they
 represent.

Afterwards, we perform spectral clustering normally on the similarity matrix. Presumably you don't want a description
 on how to perform spectral clustering, but there are many libraries to do so. The important part to note is that the
 "approximate" designation is because the similarity matrix is approximated, and LSH is done in the mapper stage.
