package edu.uga.cs.ceren.assn3

import java.awt.image.BufferedImage
import java.util.concurrent.TimeUnit

import org.apache.commons.math3.linear.{SingularValueDecomposition, MatrixUtils}
import org.apache.spark.SparkContext._
import org.apache.spark.SparkContext
import java.io._
import javax.imageio._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.storage.StorageLevel._

import util.control.Breaks._

import scala.collection.mutable.Map

/* Created by Roi on 3/9/2015.
*/
object SVD {
  def parseCmd (args: Array[String]) : Map[String, String] = {
    var toReturn = Map[String, String]()
    for(arg <- args)
    {
      //printf(arg+"\n")
        if(arg.contains("local")) {
          toReturn += ("local" -> arg)
        }
        else if(!arg.contains("master")) {
          var tokenize = arg.split("=")
          toReturn += (tokenize(0).replace("-","") -> tokenize(1))
        }
    }
    return toReturn
  }
  def rgbMatrix(s: File, k:Int): Array[Double] = {
    val img = ImageIO.read(s)
    val arr = Array.fill[Array[Double]](img.getHeight())(Array.fill[Double](img.getWidth())(0))
    //Read in image
    for(row<-0 to img.getHeight()-1; col<-0 to img.getWidth()-1)
      arr(row)(col) = ((img.getRGB(row,col)>>16) & 0xFF).toDouble

    if(k<999)
    {
      val matrix = MatrixUtils.createRealMatrix(arr)
      val svd = new SingularValueDecomposition(matrix)

      //Get decomposed vectors
      val U = svd.getU()
      val S = svd.getS()
      val VT = svd.getVT()

      //Get the first k dimensions of the vectors
      val Uk = U.getSubMatrix(0, img.getHeight()-1, 0, k-1)
      val Sk = S.getSubMatrix(0, k-1, 0, k-1)
      val VTk = VT.getSubMatrix(0, k-1, 0, img.getWidth()-1)

      return Uk.multiply(Sk).multiply(VTk).getData().flatten
    } else {
      return arr.flatten
    }
  }

  def grayscale(color: Int): Int = {
    return -16777216+(color << 16)+(color << 8) + color
  }

  def merge(ar1: Array[Double], ar2: Array[Double]): Array[Double] = {
    for(i<-0 to ar1.length-1)
      ar1(i) += ar2(i)
    return ar1
  }

  def findMin(ar1: Array[Double], ar2: Array[Double]): Array[Double] = {return findMinMax(ar1, ar2, 0)}
  def findMax(ar1: Array[Double], ar2: Array[Double]): Array[Double] = {return findMinMax(ar1, ar2, 1)}

  def findMinMax(ar1: Array[Double], ar2: Array[Double], minMax: Integer): Array[Double] = {
    val minmax: Array[Double] = Array.fill[Double](ar1.length)(0.0)
    for(i<-0 to ar1.length-1)
    {
      if(minMax==0) {
        if(ar1(i)<ar2(i)) minmax(i) = ar1(i)
        else if(ar1(i)>=ar2(i)) minmax(i) = ar2(i)
      } else if(minMax==1) {
        if(ar1(i)<ar2(i)) minmax(i) = ar2(i)
        else if(ar1(i)>=ar2(i)) minmax(i) = ar1(i)
      }
    }
    return minmax
  }

  def computeNorm(a: Array[Array[Double]], ak: Array[Array[Double]]): Double = {
    var sum = 0.0
    for(row <- 0 to a.length-1; col <- 0 to a(0).length-1)
      sum += math.pow(a(row)(col)-ak(row)(col),2)
    return math.sqrt(sum)
  }

  def printKSpans(spans: Array[Double], k: Integer, rowNum: Integer, colNum: Integer) = {
    val kSpan = Array.fill[(Integer, Double)](k)((0, 0.0))
    for(i<-0 to spans.length-1) {
      breakable {
        for(j <- 0 to kSpan.length-1) {
          if(spans(i)>kSpan(j)._2) {
            for(n <- kSpan.length-1 to j) {
              kSpan(n) = kSpan(n-1)
            }
            kSpan(j) = (i, spans(i))
            break
          }
        }
      }
    }

    kSpan.foreach(x=>printf(math.floor(x._1/colNum).toInt+","+(x._1%colNum).toInt+": "+x._2.toInt+"\n"))
  }

  def main (args: Array[String]) {
    val cmdMap = parseCmd(args)

    val ks_tuple = cmdMap.getOrElseUpdate("k", "10").split(",")
    var ks: Array[Int] = Array.fill[Int](1)(999)
    ks_tuple.foreach(x => ks +:= Integer.parseInt(x))
    val path = cmdMap.getOrElseUpdate("input", "")

    if(path.equals("")) {
      printf("Input path not provided! Terminating...")
      sys.exit(-1)
    }

    //Convert directory to list of files
    val imageDir = new File(path)
    val imageArray = imageDir.listFiles

    //Initialize averages with last file
    val dimensionImg = ImageIO.read(imageArray(0))
    val colNum = dimensionImg.getData().getWidth()
    val rowNum = dimensionImg.getData().getHeight()

    //Compute spans
    val sc_inner = new SparkContext(cmdMap.getOrElseUpdate("local", "local[*]"), "")
    val image_inner = sc_inner.parallelize(imageArray)
    val flatmaps = image_inner.map(img => rgbMatrix(img, 999))
    val mins = flatmaps.reduce(findMin)
    val maxs = flatmaps.reduce(findMax)
    val minMax = mins.zip(maxs)
    val spans = Array.fill[Double](minMax.length)(0.0)
    for(i<-0 to minMax.length-1)
      spans(i) = minMax(i)._2-minMax(i)._1
    sc_inner.stop()

    //Print top spans
    if(Integer.parseInt(cmdMap.getOrElseUpdate("kSpans", "0"))>0)
      printKSpans(spans, Integer.parseInt(cmdMap.getOrElseUpdate("kSpans", "0")), rowNum, colNum)

    //For frobineus norms
    var fmap = scala.collection.mutable.Map[Int, Array[Array[Double]]]()

    for(k <- ks)
    {
      val sc = new SparkContext(cmdMap.getOrElseUpdate("local", "local[*]"), "")

      //Create 2d array filled with 0s
      val averages = Array.fill[Array[Double]](rowNum)(Array.fill[Double](colNum)(0))
      val imgType = dimensionImg.getType()

      //Create RDD from files
      val images = sc.parallelize(imageArray)
      //val images = sc.textFile(path+"/*")
      //Begin timing
      val then = System.nanoTime()

      //Trying something new
      val innerk = sc.broadcast(k)
      val numImages = sc.broadcast(imageArray.length)
      val imgs = images.map(img => rgbMatrix(img, innerk.value)).reduce(merge)

      //Fix averages and construct image
      val reconstructedImage = new BufferedImage(rowNum, colNum, imgType)
      for(i <- 0 to averages.length-1)
      {
        for(j <- 0 to averages(0).length-1)
        {
          averages(i)(j) = imgs(i*averages.length+j)
          averages(i)(j) /= numImages.value
          //printf(averages(i)(j).toInt+" ")
          reconstructedImage.setRGB(i, j, grayscale(averages(i)(j).toInt))
        }
        //printf("\n")
      }

      fmap += (k -> averages)

      //Write image
      ImageIO.write(reconstructedImage, "png", new File("p2_"+k+".png"))

      val now = System.nanoTime()-then

      printf("\nRuntime ("+k+"): "+TimeUnit.SECONDS.convert(now, TimeUnit.NANOSECONDS)+" seconds")

      sc.stop()
    }

    //Compute Frobineus Norms
    if(cmdMap.getOrElseUpdate("norm", "0").equals("1"))
    {
      for(k<-ks)
      {
        if(k!=999)
          printf("Norm for "+k+"=10:  "+computeNorm(fmap(999),fmap(k))+"\n")
      }
    }
  }
}
